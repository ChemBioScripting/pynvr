#!/bin/bash

PROJECT=pynvr
MAJOR_VERSION=0
MINOR_VERSION=1
PACKAGE_REVISION=1

package=${PROJECT}-${MAJOR_VERSION}.${MINOR_VERSION}-${PACKAGE_REVISION}
pynvr_home=$package/usr/share/pynvr
pynvr_share=$package/usr/share/pynvr
pynvr_doc=$package/usr/share/doc/pynvr

echo "Creating $package"


sudo rm -fr $package 2>/dev/null

# Create directories
mkdir -m 0755 -p $pynvr_home
mkdir -m 0755 -p $pynvr_share
mkdir -m 0755 -p $package/usr/lib/systemd/scripts
mkdir -m 0755 -p $package/lib/systemd/system
mkdir -m 0755 -p $package/etc/init.d
mkdir -m 0755 -p $package/etc
mkdir -m 0755 $package/DEBIAN
mkdir -m 0755 -p $pynvr_doc

# Copy files places
cp *.py $pynvr_home
cp -r static $pynvr_share
cp -r shared $pynvr_home
cp -r pylive555 $pynvr_home



# Remove extra stuff
rm -fr $pynvr_home/pylive555/live
rm -fr $pynvr_home/pylive555/build
rm -fr $pynvr_home/pylive555/live555-latest.tar.gz
rm -fr $pynvr_home/pylive555/.git
rm -fr $pynvr_home/shared/__pycache__/



cp package/pynvr.initd $package/etc/init.d
cp package/pynvr.systemd $package/usr/lib/systemd/scripts
chmod 744 $package/usr/lib/systemd/scripts/pynvr


cp package/pynvr.service $package/lib/systemd/system

cp nvr.json.etc $package/etc/nvr.json
cp nvr.json.etc $pynvr_home


cp package/conffiles $package/DEBIAN
cp package/copyright $pynvr_doc
cp package/changelog.Debian $pynvr_doc
gzip -n -9 $pynvr_doc/changelog.Debian

echo "Package: $PROJECT
Version: ${MAJOR_VERSION}.${MINOR_VERSION}-${PACKAGE_REVISION}
Maintainer: Jack Strohm <hoyle.hoyle@gmail.com>
Original-Maintainer: Jack Strohm <hoyle.hoyle@gmail.com>
Architecture: all
Depends: ffmpeg (>= 2.5), python3 (>= 3.4), python3-bottle (>=0.12), adduser (>=3), liblivemedia23 (>=2014.01.13-1), liblivemedia-dev (>=2014.01.13-1)
Proviates: $PROJECT-${MAJOR_VERSION}
Section: python
Priority: optional
Homepage: http://gitlab.com/hoyle.hoyle/pynvr
Description: Python based Network Video Recorder
 PyNVR is a simple Network Video Recorder designed to record RTSP data
 streams from IP based cameras and store them as MP4 files.  It provides
 services for recording, cleaning up old files, and browsing the current
 library via a web page.
" > $package/DEBIAN/control
chmod 0644 $package/DEBIAN/control

cp package/postinst $package/DEBIAN

# Fix errors and such
chmod 0755 $package/lib
chmod 0755 $package/lib/systemd
chmod 0644 $package/lib/systemd/system/pynvr.service
rm -f $package/usr/lib/pynvr/pylive555/live/COPYING
chmod 0755 $package/usr/lib/systemd/scripts/pynvr
rm -f $package/usr/lib/pynvr/pylive555/live/configure
rm -f $package/usr/lib/pynvr/pylive555/live/fix-makefile
rm -f $package/usr/lib/pynvr/pylive555/live/genMakefiles
rm -f $package/usr/lib/pynvr/pylive555/live/genWindowsMakefiles

# Fix directory permissions
find $package -type d | while read dd
do
	chmod 0755 $dd
done

# Fix file permissions
find $package -type f | while read ff
do
	chmod g-w $ff
done


sudo chown -R root:root $package

sudo rm $package.deb 2>/dev/null
sudo dpkg-deb --build $package

echo "Checking package $package"
lintian $package.deb
